using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cut : MonoBehaviour
{

    public GameObject cubo;
    public GameObject piramide;

    private bool flag;

    // Start is called before the first frame update
    void Start()
    {
        flag = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Cuttable" && flag) {

            //print("REBANAAAAAAAAAAAAR");
            Destroy(collision.gameObject);
            if (collision.gameObject.name.StartsWith("Cube"))
            {
                var pos = collision.transform.position;
                var mat = collision.gameObject.GetComponent<MeshRenderer>().material;
                piramide.gameObject.GetComponent<MeshRenderer>().material = mat;
                Instantiate(piramide).gameObject.transform.position = new Vector3(pos.x, pos.y, pos.z);
            }
            else
            {
                var pos = collision.transform.position;
                var mat = collision.gameObject.GetComponent<MeshRenderer>().material;
                cubo.gameObject.GetComponent<MeshRenderer>().material = mat;
                cubo.name = "Cube";
                Instantiate(cubo).gameObject.transform.position = new Vector3(pos.x, pos.y, pos.z);

            }

            StartCoroutine("cooldown");

        }
        
    }

    public IEnumerator cooldown()
    {
        flag = false;
        yield return new WaitForSeconds(2);
        flag = true;
    }

}
