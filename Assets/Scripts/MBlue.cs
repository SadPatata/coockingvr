using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Samples.StarterAssets;

public class MBlue : MonoBehaviour
{

    public GameObject B;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ObjectSpawner(){

        Vector3 parent = this.transform.position;          
        Instantiate(B).gameObject.transform.position = new Vector3(parent.x, parent.y + 2, parent.z);
    }


}
