using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetrics;

public class olla : MonoBehaviour
{

    private (string, string) firstIngridient;
    public Material[] materiales;
    public Material baseMaterial;

    private bool flag;
    // Start is called before the first frame update
    void Start()
    {
        flag = true;
        firstIngridient = ("", "");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (flag && (collision.gameObject.tag == "Cuttable" || collision.gameObject.tag == "Coockable"))
        {

            //print("Base: " + baseObject);
            //print("First: " + firstIngridient);
            //print("Second: " + secondIngridient);

            if (firstIngridient.Item1.Equals(""))
            {
                firstIngridient.Item1 = collision.gameObject.GetComponent<MeshFilter>().mesh.name;
                firstIngridient.Item2 = collision.gameObject.GetComponent<Renderer>().material.name;


                Destroy(collision.gameObject);
                print(collision.gameObject.GetComponent<Renderer>().material.name);

                //print(" PRIMEROOOOOOO");
            }
            else
            {

               // print(firstIngridient.Item1 + ", " + collision.gameObject.GetComponent<MeshFilter>().mesh.name);

                StartCoroutine("cooldown");

                if (collision.gameObject.GetComponent<MeshFilter>().mesh.name.Equals(firstIngridient.Item1))
                {

                    Material newcolor = materiales[0];

                    print(collision.gameObject.GetComponent<Renderer>().material.name);
                    

                    switch (collision.gameObject.GetComponent<Renderer>().material.name)
                    {
                      
                        case "Red (Instance)":
                            switch (firstIngridient.Item2)
                            {
                                case "Red (Instance)":
                                newcolor = materiales[0];
                                break;
                                case "Blue (Instance)":
                                newcolor = materiales[3];
                                break;
                                case "Green (Instance)":
                                newcolor = materiales[5];
                                break;
                                default:
                                newcolor = collision.gameObject.GetComponent<MeshRenderer>().material;
                                break;
                            }
                            break;
                        case "Blue (Instance)":
                            switch (firstIngridient.Item2)
                            {
                                case "Red (Instance)":
                                newcolor = materiales[3];
                                break;
                                case "Blue (Instance)":
                                newcolor = materiales[2];
                                break;
                                case "Green (Instance)":
                                newcolor = materiales[4];
                                break;
                                default:
                                newcolor = collision.gameObject.GetComponent<MeshRenderer>().material;
                                break;
                            }
                            break;
                        case "Green (Instance)":
                            switch (firstIngridient.Item2)
                            {
                                case "Red (Instance)":
                                newcolor = materiales[5];
                                break;
                                case "Blue (Instance)":
                                newcolor = materiales[4];
                                break;
                                case "Green (Instance)":
                                newcolor = materiales[1];
                                break;
                                default:
                                newcolor = collision.gameObject.GetComponent<MeshRenderer>().material;
                                break;
                            }
                            break;
                        default:
                        newcolor = collision.gameObject.GetComponent<MeshRenderer>().material;
                        break;
                    }


                        print("ta bien");
                    Vector3 parent = this.transform.position;
                    collision.gameObject.GetComponent<MeshRenderer>().material = newcolor;
                    collision.gameObject.transform.position = new Vector3(parent.x, parent.y + 3, parent.z - 3);

                
                }
                else
                {

                    print("ta mal");

                    this.GetComponent<AudioSource>().Play();

                    Vector3 parent = this.transform.position;
                    collision.gameObject.GetComponent<MeshRenderer>().material = materiales[6];
                    collision.gameObject.transform.position = new Vector3(parent.x, parent.y + 3, parent.z - 3);
                }

                StartCoroutine("reset");
            }
            
        }
    }

    public IEnumerator cooldown()
    {
        flag = false;
        yield return new WaitForSeconds(5);
        flag = true;
    }

    public IEnumerator reset()
    {
        
        yield return new WaitForSeconds(2);
        firstIngridient = ("", "");

    }
}
