using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cutting_table : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        //print("holaaaaaa");
        if (collision.gameObject.tag == "Cuttable" || collision.gameObject.tag == "Coockable")
        {
            //print("COLOCAAAAR");
            collision.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
            collision.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Cuttable" || collision.gameObject.tag == "Coockable")
        {
            //print("COLOCAAAAR");
            collision.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        }
    }

}
