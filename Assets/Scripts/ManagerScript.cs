using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using static Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetrics;

public class ManagerScript : MonoBehaviour
{

    public Text pedido;
    public Toggle pedidoTogle;
    public GameObject[] formas;
    public Material[] materiales;
    public AudioClip[] audioclips;

    private (string, string) res;

    // Start is called before the first frame update
    void Start()
    {
        newOrder();

    }

    private void newOrder()
    {
        res.Item2 = materiales[UnityEngine.Random.Range(0, 6)].name;
        res.Item1 = formas[UnityEngine.Random.Range(0, 4)].transform.name;
        pedido.text = "Mete en la nevera: " + res.Item1 + " " + res.Item2;

        pedidoTogle.GetComponent<Image>().color = new Color(255f, 255f, 255f, 255f);
        
    }

    private void OnCollisionEnter(Collision collision)
    {

        print(collision.transform.name.Replace("(Clone)", "") + ", " + res.Item1);
        print(collision.gameObject.GetComponent<Renderer>().material.name.Split()[0] + ", " + res.Item2);

        if (collision.transform.name.Replace("(Clone)", "").Equals(res.Item1) && collision.gameObject.GetComponent<Renderer>().material.name.Split()[0].Equals(res.Item2))
        {
            pedidoTogle.GetComponent<Image>().color = new Color(0f, 231f, 0f, 0.8f);
            this.GetComponent<AudioSource>().clip = audioclips[0];
            this.GetComponent<AudioSource>().Play();
            pedido.text = "Bien hecho, siguiente pedido en 3s";
            StartCoroutine("cooldown");
        }
        else
        {
            this.GetComponent<AudioSource>().clip = audioclips[1];
            this.GetComponent<AudioSource>().Play();
            pedidoTogle.GetComponent<Image>().color = new Color(231f, 0f, 0f, 0.8f);
            pedido.text += "  La cagaste compa�ero";
            print("No lo has hecho bien");
        }

        Destroy(collision.gameObject);
    }


    public IEnumerator cooldown()
    {     
        yield return new WaitForSeconds(3);
        newOrder();
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
