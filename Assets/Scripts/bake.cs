using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bake : MonoBehaviour
{
    private (string, string) firstIngridient;
    public GameObject[] formas;

    private bool flag;
    // Start is called before the first frame update
    void Start()
    {
        flag = true;
        firstIngridient = ("", "");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (flag && (collision.gameObject.tag == "Cuttable" || collision.gameObject.tag == "Coockable"))
        {
       

            if (firstIngridient.Item1.Equals(""))
            {
                firstIngridient.Item1 = collision.gameObject.GetComponent<MeshFilter>().mesh.name;
                firstIngridient.Item2 = collision.gameObject.GetComponent<Renderer>().material.name;


                Destroy(collision.gameObject);
                print(collision.gameObject.GetComponent<MeshFilter>().mesh.name);

                //print(" PRIMEROOOOOOO");
            }
            else
            {

                // print(firstIngridient.Item1 + ", " + collision.gameObject.GetComponent<MeshFilter>().mesh.name);

                print(collision.gameObject.GetComponent<MeshFilter>().mesh.name);

                StartCoroutine("cooldown");

                

                GameObject newGO = formas[6];

                print(firstIngridient.Item1.Split()[0]);
                print(collision.gameObject.GetComponent<MeshFilter>().mesh.name.Split()[0]);

                if (collision.gameObject.GetComponent<Renderer>().material.name.Split()[0].Equals(firstIngridient.Item2.Split()[0]))
                {

                   
                   
                    switch (collision.gameObject.GetComponent<MeshFilter>().mesh.name.Split()[0])
                    {

                        case "Cube":
                            switch (firstIngridient.Item1.Split()[0])
                            {
                                case "Cube":
                                    newGO = formas[0];
                                    break;
                                case "pPyramid1":
                                    newGO = formas[4];
                                    break;
                                case "Sphere":
                                    newGO = formas[3];
                                    break;
                                default:
                                    newGO = collision.gameObject;
                                    break;
                            }
                            break;
                        case "pPyramid1":
                            switch (firstIngridient.Item1.Split()[0])
                            {
                                case "Cube":
                                    newGO = formas[4];
                                    break;
                                case "pPyramid1":
                                    newGO = formas[2];
                                    break;
                                case "Sphere":
                                    newGO = formas[5];
                                    break;
                                default:
                                    newGO = collision.gameObject;
                                    break;
                            }
                            break;
                        case "Sphere":
                            switch (firstIngridient.Item1.Split()[0])
                            {
                                case "Cube":
                                    newGO = formas[3];
                                    break;
                                case "pPyramid1":
                                    newGO = formas[5];
                                    break;
                                case "Sphere":
                                    newGO = formas[1];
                                    break;
                                default:
                                    newGO = collision.gameObject;
                                    break;
                            }
                            break;
                        default:
                            newGO = collision.gameObject;
                            break;
                    }


                    print("ta bien");
                    Vector3 parent = this.transform.position;
                    newGO.GetComponent<MeshRenderer>().material = collision.gameObject.GetComponent<MeshRenderer>().material;
                    newGO.transform.position = new Vector3(parent.x, parent.y + 3, parent.z - 3);


                }
                else
                {

                    print("ta mal");


                    this.GetComponent<AudioSource>().Play();
                    Vector3 parent = this.transform.position;
                    newGO = formas[6];
                    newGO.transform.position = new Vector3(parent.x, parent.y + 3, parent.z - 3);
                }

                Instantiate(newGO);
                StartCoroutine("reset");
                Destroy(collision.gameObject);
            }

        }
    }

    public IEnumerator cooldown()
    {
        flag = false;
        yield return new WaitForSeconds(5);
        flag = true;
    }

    public IEnumerator reset()
    {

        yield return new WaitForSeconds(2);
        firstIngridient = ("", "");

    }

}
