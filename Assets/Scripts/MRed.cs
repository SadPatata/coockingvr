using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MRed : MonoBehaviour
{
    public GameObject R;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ObjectSpawner()
    {

        Vector3 parent = this.transform.position;
        Instantiate(R).gameObject.transform.position = new Vector3(parent.x, parent.y + 2, parent.z);
    }
}
